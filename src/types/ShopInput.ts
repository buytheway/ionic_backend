import { Field, InputType } from "type-graphql"

@InputType()
export class ShopUnregisteredInput {
  @Field()
  name: string

  @Field()
  address: string
}
