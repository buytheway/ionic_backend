import { Field, InputType } from "type-graphql"

@InputType()
export class EditUserBasicInfoInput {
  @Field()
  tel: string

  @Field()
  address: string

  @Field()
  zip: string

  @Field()
  city: string

  @Field()
  country: string
}
