import { Field, ObjectType } from "type-graphql"
import { ShopUnregistered } from "../entities/Service"
import { User } from "../entities/User"


@ObjectType()
export class ServiceResponse {
  @Field({nullable: true})
  id?: string

  @Field(()=>User, {nullable: true})
  user?: User

  @Field(()=>ShopUnregistered, {nullable: true})
  shopUnregistered?: ShopUnregistered
}