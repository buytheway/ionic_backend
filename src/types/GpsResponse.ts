import { prop } from "@typegoose/typegoose"
import { Field, Float, ObjectType } from "type-graphql"

@ObjectType()
export class GpsCode {
  @Field(() => Float, { nullable: true })
  @prop()
  public lat?: number

  @Field(() => Float, { nullable: true })
  @prop()
  public lng?: number
}
