import { ApolloError } from "apollo-server-express"
import { createMethodDecorator } from "type-graphql"


/**
 * check if the user logged, if not, return apollo error, otherwise, continue
 */
export function logged() {
  return createMethodDecorator(async ({ context }, next) => {
    if (!(context as any).user || !(context as any).user.id) {
      return new ApolloError("please login!")
    }

    return next()
  })
}
