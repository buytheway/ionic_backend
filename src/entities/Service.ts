import { getModelForClass, modelOptions, prop } from "@typegoose/typegoose"
import { model } from "mongoose"
import { type } from "os"
import { Field, Float, ID, ObjectType } from "type-graphql"
import { User } from "./User"

@ObjectType()
@modelOptions({ schemaOptions: { timestamps: true } })
export class ShopUnregistered {
  @Field({ nullable: true })
  id?: string

  @Field()
  @prop()
  public name!: string

  @Field()
  @prop()
  public address!: string

  @Field((returns) => [Float], { nullable: true })
  @prop({ type: () => [Number] })
  public gps?: number[]

  @Field({ nullable: true })
  @prop()
  public image?: string
}

@ObjectType()
@modelOptions({ schemaOptions: { timestamps: true } })
export class Service {
  @Field((type) => ID)
  public id: string

  @Field()
  @prop({ ref: () => User })
  public user: User

  @Field((type) => ShopUnregistered)
  @prop({ type: () => ShopUnregistered })
  public shopUnregistered: ShopUnregistered
}

export const ServiceModel = getModelForClass(Service)
