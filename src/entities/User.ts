import {
  DocumentType,
  getModelForClass,
  index,
  modelOptions,
  pre,
  prop,
} from "@typegoose/typegoose"
import bcryt from "bcrypt"
import { Field, ID, ObjectType } from "type-graphql"
import { GpsCode } from "../types/GpsResponse"

export enum ERole {
  ADMIN = "admin",
  USER = "user",
  BUSINESS = "business",
}

@pre<User>("save", async function (next) {
  let user = this

  if (!user.isModified("password")) {
    return next()
  }

  try {
    user.password = await User.hashPassword(user.password)
    return next()
  } catch (error) {
    throw error
  }
})
@ObjectType()
@index({ gps: "2dsphere" })
@modelOptions({ schemaOptions: { timestamps: true } })
export class User {
  @Field(() => ID)
  id: string

  @Field(() => [String])
  @prop({ default: [ERole.USER], type: () => [String] })
  public roles!: ERole[]

  @Field()
  @prop({ required: true })
  public username!: string

  @Field({ nullable: true })
  @prop({
    default:
      "https://www.buytheway.app/wp-content/uploads/2021/03/default-user-image.png",
  })
  public avatar?: string

  @Field({ nullable: true })
  @prop({ required: true, unique: true })
  public email!: string

  @prop({ required: true })
  public password!: string

  @Field(() => String)
  @prop({ default: Date.now })
  public createdAt?: Date

  @Field({ nullable: true })
  @prop({ default: "Free" })
  public tier?: string

  @Field({ nullable: true })
  @prop()
  public tel?: string

  @Field({ nullable: true })
  @prop()
  public address?: string

  @Field({ nullable: true })
  @prop()
  public zip?: string

  @Field({ nullable: true })
  @prop()
  public city?: string

  @Field({ nullable: true })
  @prop()
  public country?: string

  @Field((type) => [Number], { nullable: true })
  @prop({ type: () => [Number] })
  public gps?: Number[]

  @Field({ nullable: true })
  @prop({ default: true })
  public activated?: boolean

  @prop()
  public static async hashPassword(originalPassword: string) {
    try {
      const hashedPassword = await bcryt.hash(originalPassword, 12)
      return hashedPassword
    } catch (error) {
      throw error
    }
  }

  public async comparePassword(
    this: DocumentType<User>,
    candidatePassword: string
  ) {
    try {
      return await bcryt.compare(candidatePassword, this.password)
    } catch (error) {
      throw error
    }
  }
}

export const UserModel = getModelForClass(User)
