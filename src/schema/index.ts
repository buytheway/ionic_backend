import { GraphQLSchema } from "graphql"
import { buildSchema } from "type-graphql"
import path from "path"
import { AuthResolver } from "../resolves/AuthResolver"
import { customAuthChecker } from "../auth/custom-auth-checker"
import { ServiceResolver } from "../resolves/ServiceResolver"
import { GpsCode } from "../types/GpsResponse"
import { UploadResolver } from "../resolves/UploadsResolver"

export default async function createSchema(): Promise<GraphQLSchema> {
  const schema = await buildSchema({
    
    resolvers: [AuthResolver, ServiceResolver, UploadResolver],
    authChecker: customAuthChecker,
    emitSchemaFile: path.resolve(__dirname, "../../schema.graphql"),
    orphanedTypes: [GpsCode],
    validate: false,
    
  })

  return schema
}
