import { Storage } from "@google-cloud/storage"
// import { GraphQLUpload } from "apollo-server-express"
// import { FileUpload } from "graphql-upload"
import { v4 as uuidV4 } from "uuid"

import { Arg, Mutation, Resolver, Query, Ctx, Authorized } from "type-graphql"
import { defaultBucket, uploadFile } from "../play/upload_to_GS"
import { File } from "../types/File"
import { User, UserModel } from "../entities/User"
import { rejects } from "assert/strict"
import { ReadStream } from "fs"
import { Stream } from "stream"
import { FileUpload, GraphQLUpload } from "graphql-upload"
import { config } from "../config"
import { deleteFileFromGS } from "../util/googleStorageUtils"
import { ApolloError } from "apollo-server-express"

// export interface FileUpload {
//   filename: string
//   mimetype: string
//   encoding: string
//   createReadStream: ()=>Stream
// }

@Resolver()
export class UploadResolver {
  @Authorized("user")
  @Mutation(() => User)
  async singleAvatarUpload(
    @Arg("file", () => GraphQLUpload) file: FileUpload,
    @Ctx("user") currentUser: any
  ) {
    console.log("Hallo, it is me!")
    console.log("file: ", file)
    const user = await UserModel.findById(currentUser.id)

    await deleteFileFromGS(user.avatar)


    const fileName = user.username + "_" + "avatar" + "_" + file.filename
    const btwBucketFile = defaultBucket.file(fileName)
    user.avatar = btwBucketFile.publicUrl()


    file
      .createReadStream()
      .pipe(
        btwBucketFile.createWriteStream({
          resumable: false,
          gzip: true,
        })
      )
      .on("finish", () => {
        user.save()
      })
      .on("error", (e) => {
        console.log(e.message)
        throw new ApolloError("something wrong happened at upload avatar")
      })
            


    return user
  }
}
