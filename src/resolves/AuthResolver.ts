import { Arg, Mutation, Resolver, Query, Ctx, Authorized } from "type-graphql"
import { ApolloError } from "apollo-server-express"
import jwt from "jsonwebtoken"

import { config } from "../config"
import { ERole, User, UserModel } from "../entities/User"
import { AuthInput, UserResponse } from "../types"
import { DocumentType } from "@typegoose/typegoose"
import chalk from "chalk"
import { Context } from "vm"
import { EditUserProfileInput } from "../types/EditUserInput"
import { EditUserBasicInfoInput } from "../types/EditBasicInfoInput"
import { getCoordsForAddress } from "../util/location"

export const signUserToken = (user: DocumentType<User>): string => {
  const payload = {
    id: user.id,
    username: user.username,
  }

  const token = jwt.sign(payload, config.jwt.secrect, { algorithm: "HS256" })

  return token
}

@Resolver()
export class AuthResolver {
  @Mutation(() => UserResponse)
  async register(
    @Arg("input") { username, email, password }: AuthInput
  ): Promise<UserResponse> {
    const existingUsername = await UserModel.findOne({ username })
    if (existingUsername) {
      throw new ApolloError(`username: ${username} is already exist`)
    }
    const existingEmail = await UserModel.findOne({ email })

    if (existingEmail) {
      throw new ApolloError(`email: ${email} is already exist`)
    }

    const user = new UserModel({ username, email, password })

    await user.save()

    const token = signUserToken(user)

    return {
      user,
      token,
    }
  }

  @Mutation(() => UserResponse)
  async login(
    @Arg("email") email: string,
    @Arg("password") password: string
  ): Promise<UserResponse> {
    try {
      const user = await UserModel.findOne({ email })

      if (!user) {
        throw new ApolloError(`${email} can not be found!`)
      }

      const logged = await user.comparePassword(password)

      if (!logged) {
        throw new ApolloError(`password is wrong`)
      }

      const token = signUserToken(user)

      return { user, token }
    } catch (error) {
      throw error
    }
  }

  @Authorized("admin")
  @Mutation(() => User, { nullable: true })
  async deleteUser(
    @Arg("userId") userId: string,
    @Ctx("user") currentUser: any
  ) {
    // You can not delete yourself
    if (userId == currentUser.id) return null

    // delete the user
    const res = await UserModel.findByIdAndRemove(userId)

    return res
  }

  @Authorized()
  @Mutation(() => User, { nullable: true })
  async editUserProfile(
    @Arg("input") input: EditUserProfileInput,
    @Ctx("user") currentUser: any
  ) {
    if (input.id !== currentUser.id) {
      throw new ApolloError("you are not allowed to change other users profile")
    }

    const userId = input.id

    const newInput: any = { ...input }
    delete newInput.id

    // if nothing changes, then you should not update
    if (newInput.username == currentUser.username) {
      delete newInput.username
    }

    if (newInput.email == currentUser.email) {
      delete newInput.email
    }

    if (Object.keys(newInput).length === 0) {
      throw new ApolloError("nothing to change")
    }

    // username should not be already used
    if (newInput.username && newInput.username !== "") {
      const res = await UserModel.findOne({ username: newInput.username })

      if (res) {
        throw new ApolloError(
          `username: ${newInput.username} already existiert`
        )
      }
    }

    // email should not be already used
    if (newInput.email && newInput.email !== "") {
      const res = await UserModel.findOne({ email: newInput.email })

      if (res) {
        throw new ApolloError(`email: ${newInput.email} already existiert`)
      }
    }

    const user = await UserModel.findByIdAndUpdate(userId, newInput)

    return user
  }

  @Authorized()
  @Mutation((type) => User)
  async editUserPassword_simple(
    @Arg("userId") userId: string,
    @Arg("newPassword") newPassword: string,
    @Ctx("user") currentUser: any
  ) {
    if (currentUser.id !== userId) {
      throw new ApolloError("You are not allowed to change other's password!")
    }

    if (newPassword == "") {
      throw new ApolloError(
        "Please give the new password, empty is not allowed!"
      )
    }

    const user = await UserModel.findByIdAndUpdate(userId, {
      password: await UserModel.hashPassword(newPassword),
    })

    return user
  }

  @Authorized()
  @Mutation((type) => User)
  async editUserPassword(
    @Arg("userId") userId: string,
    @Arg("password") password: string,
    @Arg("newPassword") newPassword: string,
    @Ctx("user") currentUser: any
  ) {
    console.log("password", password)
    console.log("newPassword", newPassword)

    if (currentUser.id !== userId) {
      throw new ApolloError("You are not allowed to change other's password!")
    }

    if (password == "") {
      throw new ApolloError(
        "Please give the new password, empty ist not allowed!"
      )
    }

    const user = await UserModel.findById(userId)

    try {
      const res = await user.comparePassword(password)
      if (!res) {
        throw new ApolloError(
          "Current Password is wrong, please give a right password !"
        )
      }

      user.password = newPassword
      await user.save()
      return user
    } catch (err) {
      throw new ApolloError("Something wrong with the compare password!")
    }
  }

  //TODO: late will require email confirmation!!!
  //TODO: only the owner and admin can change
  @Authorized()
  @Mutation(() => User, { nullable: true })
  async editUserEmail(
    @Arg("email") email: string,
    @Ctx("user") currentUser: any
  ) {
    if (currentUser.email === email) {
      return null
    }

    const existingUser = await UserModel.findOne({ email })

    if (existingUser) {
      throw new ApolloError(
        `email: ${email} is already exist, please choose to a new email`
      )
    }

    const user = await UserModel.findOne({ _id: currentUser.id })

    user.email = email
    await user.save()

    return user
  }

  @Authorized([ERole.USER])
  @Mutation(() => User, { nullable: true })
  async editUserBasicInfo(
    @Arg("input") input: EditUserBasicInfoInput,
    @Ctx("user") currentUser: any
  ) {
    const user = await UserModel.findOne({ _id: currentUser.id })

    if (!user) {
      null
    }

    user.tel = input.tel
    user.address = input.address
    user.zip = input.zip
    user.city = input.city
    user.country = input.country

    const address = `${user.address}, ${user.zip} ${user.city}, ${user.country}`

    const gps = await getCoordsForAddress(address)
    user.gps = [gps.lat, gps.lng]
    await user.save()

    return user
  }

  // @Query(() => String)
  // hello(@Arg("me") me: string, @Ctx("user") user: any) {
  //   // console.log(chalk.blue(JSON.stringify(user)))
  //   console.log(chalk.yellow("me: " + me))
  //   return "aaa"
  // }

  @Query(() => String)
  hello(@Arg("me") me: string, @Ctx("user") user: any) {
    // console.log(chalk.blue(JSON.stringify(user)))
    console.log(chalk.yellow("me: " + me))
    return "aaa"
  }

  @Query(() => User, { nullable: true })
  async currentUser(@Ctx() ctx: Context): Promise<DocumentType<User> | null> {
    if (!ctx.req.user) {
      return null
    }

    const user = await UserModel.findById(ctx.req.user.id)

    if (user) {
      return user
    } else {
      return null
    }
  }
}
