import { Arg, Authorized, Ctx, Mutation, Query, Resolver } from "type-graphql"
import { ServiceModel, ShopUnregistered } from "../entities/Service"
import { UserModel } from "../entities/User"
import { GpsCode } from "../types/GpsResponse"
import { ServiceResponse } from "../types/ServiceResponse"
import { ShopUnregisteredInput } from "../types/ShopInput"
import { getCoordsForAddress } from "../util/location"

@Resolver()
export class ServiceResolver {
  @Authorized()
  @Mutation((type) => ServiceResponse, { nullable: true })
  async createService(
    @Arg("shopUnregistered") ShopUnregisteredInput: ShopUnregisteredInput,
    @Ctx("user") currentUser: any
  ): Promise<ServiceResponse> {
    const user = await UserModel.findById(currentUser.id)

    const { lat, lng } = await getCoordsForAddress(
      ShopUnregisteredInput.address
    )

    const shopUnregistered: ShopUnregistered = {
      name: ShopUnregisteredInput.name,
      address: ShopUnregisteredInput.address,
      gps: [lat, lng],
    }

    const myService = await ServiceModel.create({
      user: user!,
      shopUnregistered: shopUnregistered,
    })

    await myService.save()

    return {
      id: myService.id,
      user: user!,
      shopUnregistered: myService.shopUnregistered,
    }
  }

  @Authorized()
  @Query((type) => GpsCode, { nullable: true })
  async getGpsFromAddress(@Arg("address") address: string) {
    const res = await getCoordsForAddress(address)
    return res
  }
}
