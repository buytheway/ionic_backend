import axios from "axios"
import { response } from "express"
import createError from "http-errors"
import { config } from "../config"
import { GpsCode } from "../types/GpsResponse"

export const getCoordsForAddress = async (address: string) => {
  const response = await axios.get(
    `https://maps.googleapis.com/maps/api/geocode/json?address=${encodeURIComponent(
      address
    )}&key=${config.google_api_key.map}`
  )
  const data = response.data

  if (!data || data.status === "ZERO_RESULTS") {
    const err = createError(
      422,
      "Could not find the location for the specified address"
    )
    throw err
  }

  console.log("res: " + data.results[0].geometry.location)

  const coordinates: GpsCode = data.results[0].geometry.location

  return coordinates
}
