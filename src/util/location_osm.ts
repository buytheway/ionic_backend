import axios from "axios"
import createHttpError from "http-errors"

export const getCoordsForAddress = async (address: string) => {
  const response = await axios.get(
    `https://nominatim.openstreetmap.org/search?q=${address}&format=geojson`
  )

  const data = response.data

  if (!data || data.features.length < 1) {
    const err = createHttpError(
      422,
      "Could not find the location for the specified address"
    )

    throw err
  }

  const [lng, lat] = data.features[0].geometry.coordinates

  return { lat, lng }
}
