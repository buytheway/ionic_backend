import { Storage, Bucket } from "@google-cloud/storage"
import chalk from "chalk"
import path from "path"
import { resolve } from "path/posix"
import { fileURLToPath, URL } from "url"
import { stringify, v4 as uuidV4 } from "uuid"
import { config } from "../config"
// const GCKey = "../buytheway-317010-be78fa6b5e3b.json"
// const projectId = "buytheway-317010"
// const bucketName = "buytheway"

const gc = new Storage({
  keyFilename: config.google_cloud_storage.gckey,
  projectId: config.google_cloud_storage.projectId,
})

const bucketName = config.google_cloud_storage.bucketName
export const defaultBucket = gc.bucket(bucketName)

/**
 * save a file into google storage and return its url
 * @param filePath the file of the local system path
 * @param newFileName the file name in the google storage
 * @param bucket
 * @returns the file url
 */
export const upload2GS = async (
  filePath: string,
  newFileName: string | undefined = undefined,
  bucket: Bucket = defaultBucket
) => {
  const destination = newFileName
    ? newFileName
    : uuidV4() + "_" + path.basename(filePath)

  const res = await bucket.upload(filePath, {
    destination,
  })

  const fileURL = new URL(
    res[0].metadata.bucket,
    res[0].storage.apiEndpoint
  ).href
    .concat("/")
    .concat(res[0].name)

  return fileURL
}

/**
 * Delete a googleStroage File from its URL
 * @param fileURL a fileURL string path from Google Storage
 * @param bucket Bucket
 * @returns the information of the deleted result
 */
export const deleteFileFromGS = async (
  fileURL: string,
  bucket: Bucket = defaultBucket
) => {
  if (!fileURL.startsWith("https://storage.googleapis.com")) {
    return false
  }
  const fileName = fileURL.substring(fileURL.lastIndexOf("/") + 1)

  try {
    await bucket.file(fileName).delete()
  } catch (err) {
    console.log("delete file error: ", err.errors[0].message)
    return false
  }
  return true
}

// usage Example:
// upload2GS("/Users/yinggu/design/buytheway/test/PNG/white_cat.png")
//   .then((res) => {
//     console.log(res)
//   })
//   .catch((res) => {
//     console.log(res)
//   })

// usage Example:
// deleteFileFromGS(
//   "https://storage.googleapis.com/buytheway/1df150e6-2232-4b97-b56e-ed5716385396_white_cat.png"
// )
//   .then((res) => {
//     console.log(res)
//   })
//   .catch((err) => {
//     console.log(chalk.redBright(err))
//   })
